package com.buily.moviedb.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Movie implements Parcelable {

    @Expose
    private String title;

    @SerializedName("release_date")
    @Expose
    private String releaseDate;

    @SerializedName("original_language")
    @Expose
    private String language;

    @SerializedName("poster_path")
    @Expose
    private String imagePoster;

    @SerializedName("backdrop_path")
    @Expose
    private String imageBackdrop;

    @SerializedName("overview")
    @Expose
    private String description;

    @Expose
    private Long id;

    @Expose
    private Double popularity;

    @SerializedName("vote_average")
    @Expose
    private Double rate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getImagePoster() {
        return imagePoster;
    }

    public void setImagePoster(String imagePoster) {
        this.imagePoster = imagePoster;
    }

    public String getImageBackdrop() {
        return imageBackdrop;
    }

    public void setImageBackdrop(String imageBackdrop) {
        this.imageBackdrop = imageBackdrop;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }


    public Movie() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.releaseDate);
        dest.writeString(this.language);
        dest.writeString(this.imagePoster);
        dest.writeString(this.imageBackdrop);
        dest.writeString(this.description);
        dest.writeValue(this.id);
        dest.writeValue(this.popularity);
        dest.writeValue(this.rate);
    }

    protected Movie(Parcel in) {
        this.title = in.readString();
        this.releaseDate = in.readString();
        this.language = in.readString();
        this.imagePoster = in.readString();
        this.imageBackdrop = in.readString();
        this.description = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.popularity = (Double) in.readValue(Double.class.getClassLoader());
        this.rate = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
