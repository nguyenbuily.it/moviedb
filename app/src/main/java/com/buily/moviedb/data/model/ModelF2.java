package com.buily.moviedb.data.model;

public class ModelF2<T1, T2> {

    private T1 response1;
    private T2 response2;

    public ModelF2(T1 response1, T2 response2) {
        this.response1 = response1;
        this.response2 = response2;
    }

    public T1 getResponse1() {
        return response1;
    }

    public void setResponse1(T1 response1) {
        this.response1 = response1;
    }

    public T2 getResponse2() {
        return response2;
    }

    public void setResponse2(T2 response2) {
        this.response2 = response2;
    }
}
