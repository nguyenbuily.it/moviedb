package com.buily.moviedb.data.source.remote.service;

import com.buily.moviedb.variable.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceClient {

    private static final int READ_TIMEOUT = 25000;
    private static final int WRITE_TIMEOUT = 25000;
    private static final int CONNECT_TIMEOUT = 25000;

    private static MovieAPI sInstance = null;

    public static synchronized MovieAPI getInstance() {
        if (sInstance == null) {
            sInstance = getService();
        }

        return sInstance;
    }

    private static MovieAPI getService() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .interceptors().add(chain -> {
                    Request request = chain.request();

                    HttpUrl url = request
                            .url()
                            .newBuilder()
                            .addQueryParameter("api_key", Constants.API_KEY).build();

                    request = request.newBuilder().url(url).build();

                    return chain.proceed(request);
                });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL_MOVIE)
                .client(builder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(MovieAPI.class);

    }
}
