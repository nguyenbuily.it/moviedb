package com.buily.moviedb.data.response;

import com.buily.moviedb.data.model.Cast;
import com.buily.moviedb.data.model.Movie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResponse {

    @Expose
    private Long id;

    @SerializedName("results")
    @Expose
    private List<Movie> listMovies;

    public List<Movie> getListMovies() {
        return listMovies;
    }

    @SerializedName("cast")
    @Expose
    private List<Cast> listCasts;

    public List<Cast> getListCasts() {
        return listCasts;
    }
}
