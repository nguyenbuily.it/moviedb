package com.buily.moviedb.data.source.repository;

import com.buily.moviedb.data.response.MovieResponse;
import com.buily.moviedb.data.response.YoutubeResponse;
import com.buily.moviedb.data.source.remote.IMovieRemoteDataSource;
import com.buily.moviedb.data.source.remote.datasource.MovieRemoteDataSource;

import io.reactivex.Observable;

public class MovieRepository implements IMovieRemoteDataSource {

    private static MovieRepository sInstance = null;
    private MovieRemoteDataSource mDataSource;

    private MovieRepository(MovieRemoteDataSource dataSource) {
        mDataSource = dataSource;
    }

    public static MovieRepository getInstance() {
        if (sInstance == null) {
            sInstance = new MovieRepository(MovieRemoteDataSource.getInstance());
        }

        return sInstance;
    }

    @Override
    public Observable<MovieResponse> getMovies(String type, int page) {
        return mDataSource.getMovies(type, page);
    }

    @Override
    public Observable<MovieResponse> getListCasts(Long movieId) {
        return mDataSource.getListCasts(movieId);
    }

    @Override
    public Observable<YoutubeResponse> getYoutubeKey(Long movieId) {
        return mDataSource.getYoutubeKey(movieId);
    }
}
