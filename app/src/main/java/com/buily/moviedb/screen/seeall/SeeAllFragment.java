package com.buily.moviedb.screen.seeall;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.base.ILoadMore;
import com.buily.moviedb.base.LoadMoreAdapter;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.databinding.FragmentSeeAllBinding;
import com.buily.moviedb.utils.FromType;
import com.buily.moviedb.variable.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SeeAllFragment extends BaseFragment<FragmentSeeAllBinding> {

    private SeeAllViewModel mSeeAllViewModel;

    private String fromType;
    private String typeMovie;

    private int page = 1;
    private boolean isLoadMore;

    private List<Movie> data;


    public static SeeAllFragment newInstance(String type) {

        Bundle args = new Bundle();
        args.putString(Constants.FROM_TYPE, type);

        SeeAllFragment fragment = new SeeAllFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_see_all;
    }

    @Override
    protected void initData() {

        data = new ArrayList<>();

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(Constants.FROM_TYPE)) {
                fromType = bundle.getString(Constants.FROM_TYPE);
            }
        }

        LoadMoreAdapter<Movie> adapter =
                new LoadMoreAdapter<>(mFmBinding.rcvSeeAll, R.layout.item_see_all_movie);
        mFmBinding.rcvSeeAll.setAdapter(adapter);
        adapter.setLoadMore(() -> {
            page++;
            isLoadMore = true;
            data.add(null);
            adapter.notifyItemInserted(data.size() - 1);
            mSeeAllViewModel.isLoadMore.set(true);
            mSeeAllViewModel.getMovies(typeMovie, page);

        });

        mSeeAllViewModel = new ViewModelProvider(this).get(SeeAllViewModel.class);
        mSeeAllViewModel.getListMovie().observe(this, movies -> {

            if (isLoadMore) {
                data.remove(data.size() - 1);
                adapter.notifyItemRemoved(data.size() - 1);
                adapter.setLoaded();
                data.addAll(movies);

            } else {
                mFmBinding.swipeRefresh.setRefreshing(false);
                data.clear();
                data.addAll(movies);
                adapter.setData(data);
            }

        });

        mFmBinding.setViewModel(mSeeAllViewModel);

        initView();
    }

    private void initView() {

        mFmBinding.swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        mFmBinding.swipeRefresh.setOnRefreshListener(() -> {

            isLoadMore = false;
            mSeeAllViewModel.getMovies(typeMovie, 1);
        });

        mActivity.setSupportActionBar(mFmBinding.toolbar);
        mFmBinding.toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        Objects.requireNonNull(mActivity.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        mFmBinding.toolbar.setNavigationOnClickListener(v -> mActivity.onBackPressed());

        Drawable iconBack = getResources().getDrawable(R.drawable.ic_back_black_24dp);
        Objects.requireNonNull(iconBack)
                .setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        Objects.requireNonNull(mActivity.getSupportActionBar())
                .setHomeAsUpIndicator(iconBack);

        switch (fromType) {
            case FromType.TypeMovie.RECOMMEND:
                typeMovie = mContext.getString(R.string.type_recommend);
                mFmBinding.toolbar.setTitle(mContext.getString(R.string.label_recommend));
                break;
            case FromType.TypeMovie.POPULAR:
                typeMovie = mContext.getString(R.string.type_popular);
                mFmBinding.toolbar.setTitle(mContext.getString(R.string.label_popular));
                break;
            case FromType.TypeMovie.TOP_RATE:
                typeMovie = mContext.getString(R.string.type_top_rate);
                mFmBinding.toolbar.setTitle(mContext.getString(R.string.label_top_rate));
                break;
            case FromType.TypeMovie.NOW_PLAYING:
                typeMovie = mContext.getString(R.string.type_now_playing);
                mFmBinding.toolbar.setTitle(mContext.getString(R.string.label_now_playing));
                break;
            case FromType.TypeMovie.UPCOMING:
                typeMovie = mContext.getString(R.string.type_upcoming);
                mFmBinding.toolbar.setTitle(mContext.getString(R.string.label_upcoming));
                break;
        }
        mSeeAllViewModel.isLoadingFirst.set(true);
        mSeeAllViewModel.getMovies(typeMovie, page);
    }
}
