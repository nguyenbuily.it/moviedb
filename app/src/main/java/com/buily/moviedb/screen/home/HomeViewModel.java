package com.buily.moviedb.screen.home;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseConsumer;
import com.buily.moviedb.base.BaseViewModel;
import com.buily.moviedb.data.model.ModelF6;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.data.source.repository.MovieRepository;
import com.buily.moviedb.data.response.MovieResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends BaseViewModel {

    private MovieRepository mRepository;

    private MutableLiveData<ModelF6<List<Movie>, List<Movie>,
            List<Movie>, List<Movie>, List<Movie>, List<Movie>>> mMovieResponse;

    public ObservableBoolean isLoading;

    private List<Movie> mListTrending;
    private List<Movie> mListRecommend;
    private List<Movie> mListPopular;
    private List<Movie> mListTopRate;
    private List<Movie> mListNowPlaying;
    private List<Movie> mListUpcoming;

    public HomeViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void init() {
        mRepository = MovieRepository.getInstance();

        mMovieResponse = new MutableLiveData<>();

        isLoading = new ObservableBoolean();

        mListTrending = new ArrayList<>();
        mListRecommend = new ArrayList<>();
        mListPopular = new ArrayList<>();
        mListTopRate = new ArrayList<>();
        mListNowPlaying = new ArrayList<>();
        mListUpcoming = new ArrayList<>();

        setUpData();
    }

    private void setUpData() {

        isLoading.set(true);

        Disposable disposable = Observable.zip(
                getMovies(mContext.getString(R.string.type_trending), 1), // rp1 - trending
                getMovies(mContext.getString(R.string.type_recommend), 2), // rp2 - recommend
                getMovies(mContext.getString(R.string.type_popular), 1), // rp3 - popular
                getMovies(mContext.getString(R.string.type_top_rate), 3), // rp4 - topRate
                getMovies(mContext.getString(R.string.type_now_playing), 1), // rp5 - nowPlaying
                getMovies(mContext.getString(R.string.type_upcoming), 1), // rp6 - upcoming
                ModelF6::new)
                .subscribeWith(new BaseConsumer<ModelF6<MovieResponse, MovieResponse,
                        MovieResponse, MovieResponse, MovieResponse, MovieResponse>>() {
                    @Override
                    protected void onSuccess(ModelF6<MovieResponse, MovieResponse, MovieResponse,
                            MovieResponse, MovieResponse, MovieResponse> object) {

                        if (object != null) {
                            if (object.getResponse1().getListMovies().size() >= 3) {
                                for (int i = 0; i < 3; i++) {
                                    mListTrending.add(object.getResponse1().getListMovies().get(i));
                                }

                            }
                            if (object.getResponse2().getListMovies().size() >= 3) {
                                for (int i = 0; i < 3; i++) {
                                    mListRecommend.add(object.getResponse2().getListMovies().get(i));
                                }
                            }
                            if (object.getResponse3().getListMovies().size() >= 3) {
                                for (int i = 0; i < 3; i++) {
                                    mListPopular.add(object.getResponse3().getListMovies().get(i));
                                }
                            }
                            if (object.getResponse4().getListMovies().size() >= 3) {
                                for (int i = 0; i < 3; i++) {
                                    mListTopRate.add(object.getResponse4().getListMovies().get(i));
                                }
                            }
                            if (object.getResponse5().getListMovies().size() >= 3) {
                                for (int i = 0; i < 3; i++) {
                                    mListNowPlaying.add(object.getResponse5().getListMovies().get(i));
                                }
                            }
                            if (object.getResponse6().getListMovies().size() >= 3) {
                                for (int i = 0; i < 3; i++) {
                                    mListUpcoming.add(object.getResponse6().getListMovies().get(i));
                                }
                            }

                            mMovieResponse.setValue(new ModelF6<>(
                                    mListTrending, mListRecommend, mListPopular,
                                    mListTopRate, mListNowPlaying, mListUpcoming));

                        }


                    }

                    @Override
                    protected void onError(String error) {
                        Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        isLoading.set(false);
                    }
                });

        mCompositeDisposable.add(disposable);

    }

    public MutableLiveData<ModelF6<List<Movie>, List<Movie>, List<Movie>,
            List<Movie>, List<Movie>, List<Movie>>> getMovieResponse() {

        return mMovieResponse;
    }

    // call api
    private Observable<MovieResponse> getMovies(String type, int page) {
        return mRepository.getMovies(type, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

}
