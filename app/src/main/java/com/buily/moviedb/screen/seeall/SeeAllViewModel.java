package com.buily.moviedb.screen.seeall;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import com.buily.moviedb.base.BaseConsumer;
import com.buily.moviedb.base.BaseViewModel;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.data.response.MovieResponse;
import com.buily.moviedb.data.source.repository.MovieRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SeeAllViewModel extends BaseViewModel {

    public ObservableBoolean isLoadingFirst;
    public ObservableBoolean showListMovie;
    public ObservableBoolean isLoadMore;

    private MovieRepository mRepository;

    private MutableLiveData<List<Movie>> mListMovie;

    public SeeAllViewModel(@NonNull Application application) {
        super(application);

    }

    @Override
    public void init() {

        isLoadingFirst = new ObservableBoolean();
        showListMovie = new ObservableBoolean();
        isLoadMore = new ObservableBoolean();

        mRepository = MovieRepository.getInstance();
        mListMovie = new MutableLiveData<>();

    }

    // call api

    public void getMovies(String type, int page) {

        if (isLoadMore.get()) {
            showListMovie.set(true);
        } else {
            showListMovie.set(false);
        }
        Disposable disposable =
                mRepository
                        .getMovies(type, page)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(new BaseConsumer<MovieResponse>() {
                            @Override
                            protected void onSuccess(MovieResponse object) {
                                mListMovie.setValue(object.getListMovies());
                            }

                            @Override
                            protected void onError(String error) {
                                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete() {
                                super.onComplete();
                                isLoadingFirst.set(false);
                                showListMovie.set(true);
                            }
                        });

        mCompositeDisposable.add(disposable);
    }

    public MutableLiveData<List<Movie>> getListMovie() {
        return mListMovie;
    }
}
