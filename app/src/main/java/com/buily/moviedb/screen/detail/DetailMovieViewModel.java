package com.buily.moviedb.screen.detail;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import com.buily.moviedb.base.BaseConsumer;
import com.buily.moviedb.base.BaseViewModel;
import com.buily.moviedb.data.model.Cast;
import com.buily.moviedb.data.model.ModelF2;
import com.buily.moviedb.data.response.MovieResponse;
import com.buily.moviedb.data.response.YoutubeResponse;
import com.buily.moviedb.data.source.repository.MovieRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DetailMovieViewModel extends BaseViewModel {

    private MovieRepository mMovieRepository;

    private MutableLiveData<List<Cast>> mMutableListCasts;

    public ObservableBoolean isPlay;
    public ObservableBoolean isPrepareDone;
    public ObservableBoolean isVideo;

    private String ytbKey;

    public DetailMovieViewModel(@NonNull Application application) {
        super(application);
    }

    @Override
    public void init() {

        mMovieRepository = MovieRepository.getInstance();

        mMutableListCasts = new MutableLiveData<>();

        isPlay = new ObservableBoolean();
        isPrepareDone = new ObservableBoolean();
        isVideo = new ObservableBoolean();

    }

    public MutableLiveData<List<Cast>> getMutableListCasts() {
        return mMutableListCasts;
    }

    public void setButtonPLayVisibility(boolean b) {
        isPlay.set(b);
    }

    public String getYtbKey() {
        return ytbKey;
    }

    //call api
    public void prepareData(Long movieId) {

        Disposable disposable = Observable
                .zip(getListCasts(movieId), getYtbKey(movieId), ModelF2::new)
                .subscribeWith(new BaseConsumer<ModelF2<MovieResponse, YoutubeResponse>>() {
                    @Override
                    protected void onSuccess(ModelF2<MovieResponse, YoutubeResponse> object) {

                        if (object.getResponse1() != null) {
                            mMutableListCasts.setValue(object.getResponse1().getListCasts());
                        }

                        if (!object.getResponse2().getListYoutube().isEmpty()) {
                            ytbKey = object.getResponse2().getListYoutube().get(0).getKey();
                            isVideo.set(true);
                        } else {
                            isVideo.set(false);
                        }
                    }

                    @Override
                    protected void onError(String error) {
                        Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        isPrepareDone.set(true);
                    }
                });

        mCompositeDisposable.add(disposable);

    }

    private Observable<MovieResponse> getListCasts(Long movieId) {
        return mMovieRepository
                .getListCasts(movieId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    private Observable<YoutubeResponse> getYtbKey(Long movieId) {
        return mMovieRepository
                .getYoutubeKey(movieId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

}
