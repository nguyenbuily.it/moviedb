package com.buily.moviedb.screen.fav;

import android.os.Bundle;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.databinding.FragmentFavoriteBinding;

public class FavoriteFragment extends BaseFragment<FragmentFavoriteBinding> {

    public static FavoriteFragment newInstance() {

        Bundle args = new Bundle();

        FavoriteFragment fragment = new FavoriteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_favorite;
    }

    @Override
    protected void initData() {

    }
}
