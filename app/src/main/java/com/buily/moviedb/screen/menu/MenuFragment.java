package com.buily.moviedb.screen.menu;

import android.os.Bundle;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.databinding.FragmentMenuBinding;

public class MenuFragment extends BaseFragment<FragmentMenuBinding> {

    public static MenuFragment newInstance() {

        Bundle args = new Bundle();

        MenuFragment fragment = new MenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_menu;
    }

    @Override
    protected void initData() {

    }
}
