package com.buily.moviedb.screen.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.buily.moviedb.base.ItemRecycleViewListener;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.databinding.ItemSlideShowBinding;

import java.util.List;

public class SlideShowAdapter extends PagerAdapter {

    private List<Movie> data;
    private LayoutInflater inflater;
    private ItemRecycleViewListener listener;

    public SlideShowAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setData(List<Movie> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(ItemRecycleViewListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        ItemSlideShowBinding binding =
                ItemSlideShowBinding.inflate(inflater, container, false);

        binding.setListener(listener);
        binding.setData(data.get(position));
        container.addView(binding.getRoot(), 0);

        return binding.getRoot();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
