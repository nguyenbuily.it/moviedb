package com.buily.moviedb.screen.search;

import android.os.Bundle;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.databinding.FragmentSearchBinding;

public class SearchFragment extends BaseFragment<FragmentSearchBinding> {

    public static SearchFragment newInstance() {

        Bundle args = new Bundle();

        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_search;
    }

    @Override
    protected void initData() {

    }
}
