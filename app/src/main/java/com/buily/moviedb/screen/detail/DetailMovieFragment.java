package com.buily.moviedb.screen.detail;

import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.buily.moviedb.R;
import com.buily.moviedb.base.BaseFragment;
import com.buily.moviedb.base.BaseRecycleViewAdapter;
import com.buily.moviedb.data.model.Cast;
import com.buily.moviedb.data.model.Movie;
import com.buily.moviedb.databinding.FragmentDetailMovieBinding;
import com.buily.moviedb.variable.Constants;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

public class DetailMovieFragment extends BaseFragment<FragmentDetailMovieBinding> {

    private DetailMovieViewModel mDetailMovieViewModel;

    private BaseRecycleViewAdapter<Cast> mCatsAdapter;

    private boolean isPlaying;

    public static DetailMovieFragment newInstance(Bundle args) {

        DetailMovieFragment fragment = new DetailMovieFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_detail_movie;
    }

    @Override
    protected void initData() {

        Bundle bundle = getArguments();

        mDetailMovieViewModel = new ViewModelProvider(this).get(DetailMovieViewModel.class);
        mFmBinding.setViewModel(mDetailMovieViewModel);

        mDetailMovieViewModel.getMutableListCasts().observe(this, casts -> mCatsAdapter.setData(casts));

        mCatsAdapter = new BaseRecycleViewAdapter<>(mContext, R.layout.item_cast);
        mFmBinding.rcvCasts.setAdapter(mCatsAdapter);

        if (bundle != null) {
            if (bundle.containsKey(Constants.PUT_OBJECT)) {
                Movie movie = bundle.getParcelable(Constants.PUT_OBJECT);
                mFmBinding.setData(movie);

                assert movie != null;
                mDetailMovieViewModel.prepareData(movie.getId());
            }
        }

        mFmBinding.imgBack.setOnClickListener(v -> {
            if (isPlaying) {
                for (Fragment fm : mActivity.getSupportFragmentManager().getFragments()) {
                    if (fm instanceof YouTubePlayerSupportFragment) {
                        mActivity.getSupportFragmentManager().beginTransaction()
                                .remove(fm)
                                .commitAllowingStateLoss();
                    }
                }
                isPlaying = false;
                mDetailMovieViewModel.setButtonPLayVisibility(false);
            } else {
                mActivity.onBackPressed();
            }
        });

        mFmBinding.imgPlay.setOnClickListener(v -> initYoutubePlayer());

    }

    private void initYoutubePlayer() {
        mDetailMovieViewModel.isPlay.set(true);
        YouTubePlayerSupportFragment playerSupportFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.youtube, playerSupportFragment);
        transaction.commit();

        playerSupportFragment.initialize(Constants.DEVELOP_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    isPlaying = true;

                    youTubePlayer.loadVideo(mDetailMovieViewModel.getYtbKey());
                    youTubePlayer.play();
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                isPlaying = false;
                Toast.makeText(mContext, youTubeInitializationResult.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
