package com.buily.moviedb.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.buily.moviedb.BR;

import java.util.List;

public class BaseRecycleViewAdapter<T> extends
        RecyclerView.Adapter<BaseRecycleViewAdapter.ViewHolder> {

    @LayoutRes
    private int layoutId;
    private LayoutInflater inflater;
    private List<T> listDatas;

    private ItemRecycleViewListener<T> listener;

    public BaseRecycleViewAdapter(Context context, int layoutId) {
        this.layoutId = layoutId;
        inflater = LayoutInflater.from(context);
    }

    public BaseRecycleViewAdapter(Context context, int layoutId, List<T> data) {
        this.layoutId = layoutId;
        inflater = LayoutInflater.from(context);
        this.listDatas = data;
    }

    public void setData(List<T> data) {
        this.listDatas = data;
        notifyDataSetChanged();
    }

    public void setListener(ItemRecycleViewListener<T> listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(inflater, layoutId,
                parent, false);
        return new ViewHolder(viewDataBinding);
    }

    @Override
    public int getItemCount() {
        return listDatas != null ? listDatas.size() : 0;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseRecycleViewAdapter.ViewHolder holder, int position) {
        holder.bindData(listDatas.get(position));
        holder.listener();

        holder.binding.executePendingBindings();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding binding;

        public ViewHolder(@NonNull ViewDataBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        private void bindData(T item) {
            binding.setVariable(BR.item, item);
        }

        private void listener() {
            binding.setVariable(BR.listener, listener);
        }
    }
}
