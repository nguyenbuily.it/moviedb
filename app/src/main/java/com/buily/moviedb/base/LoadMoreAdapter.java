package com.buily.moviedb.base;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.buily.moviedb.BR;
import com.buily.moviedb.databinding.ItemLoadingBinding;

import java.util.List;

public class LoadMoreAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_ITEM = 0;
    private final int TYPE_LOADING = 1;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem;
    private int totalCount;

    private ILoadMore mLoadMore;
    private ItemRecycleViewListener<T> mListener;

    @LayoutRes
    private int layoutId;

    private List<T> data;

    public void setLoadMore(ILoadMore mLoadMore) {
        this.mLoadMore = mLoadMore;
    }

    public void setData(List<T> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(ItemRecycleViewListener<T> mListener) {
        this.mListener = mListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public LoadMoreAdapter(RecyclerView recyclerView, int layoutId) {

        this.layoutId = layoutId;

        LinearLayoutManager linearLayoutManager =
                (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                assert linearLayoutManager != null;
                totalCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mLoadMore != null) {
                        mLoadMore.onLoadMore();
                        isLoading = true;
                    }
                }
            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) == null ? TYPE_LOADING : TYPE_ITEM;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            ViewDataBinding binding =
                    DataBindingUtil.inflate(LayoutInflater
                            .from(parent.getContext()), layoutId, parent, false);

            return new ItemViewHolder(binding);

        } else if (viewType == TYPE_LOADING) {
            ItemLoadingBinding itemLoadingBinding =
                    ItemLoadingBinding.inflate(LayoutInflater
                            .from(parent.getContext()), parent, false);

            return new LoadingViewHolder(itemLoadingBinding);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ((ItemViewHolder) holder).bindData(data.get(position));
            ((ItemViewHolder) holder).setListener();
        } else if (holder instanceof LoadingViewHolder) {
            ((LoadingViewHolder) holder).binding.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


    public class LoadingViewHolder<T> extends RecyclerView.ViewHolder {
        private ItemLoadingBinding binding;

        public LoadingViewHolder(@NonNull ItemLoadingBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }

    public class ItemViewHolder<T> extends RecyclerView.ViewHolder {

        private ViewDataBinding binding;

        public ItemViewHolder(@NonNull ViewDataBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        private void bindData(T item) {
            binding.setVariable(BR.item, item);
        }

        private void setListener() {
            binding.setVariable(BR.listener, mListener);
        }
    }

}


