package com.buily.moviedb.base;

public interface ILoadMore {

    void onLoadMore();
}
