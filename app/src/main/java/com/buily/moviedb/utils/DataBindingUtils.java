package com.buily.moviedb.utils;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.buily.moviedb.R;
import com.buily.moviedb.variable.Constants;
import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;

public class DataBindingUtils {

    @BindingAdapter("app:viewPagerAdapter")
    public static void setViewPagerAdapter(ViewPager viewPagerAdapter, PagerAdapter pagerAdapter) {
        viewPagerAdapter.setAdapter(pagerAdapter);
        viewPagerAdapter.setPageMargin(80);
        viewPagerAdapter.setPadding(120, 0, 120, 0);
    }

    @BindingAdapter("app:setRecyclerViewAdapter")
    public static void setRecyclerViewAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("app:loadImage")
    public static void loadImage(AppCompatImageView imageView, String url) {
        url = Constants.BASE_URL_IMAGE + url;
        Glide.with(imageView)
                .load(url)
                .placeholder(R.drawable.ic_film_reel)
                .error(R.drawable.ic_film_reel)
                .centerCrop()
                .into(imageView);
    }

    @BindingAdapter("app:loadCircleImage")
    public static void loadCircleImage(CircleImageView imageView, String url) {
        url = Constants.BASE_URL_IMAGE + url;
        Glide.with(imageView)
                .load(url)
                .placeholder(R.drawable.ic_film_reel)
                .error(R.drawable.ic_film_reel)
                .centerCrop()
                .into(imageView);
    }

    @BindingAdapter("app:rating")
    public static void setRating(AppCompatRatingBar rating, float value) {
        rating.setRating(value / 2);
    }
}
