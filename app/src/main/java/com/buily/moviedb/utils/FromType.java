package com.buily.moviedb.utils;

public interface FromType {

    @interface TypeMovie {
        String RECOMMEND = "RECOMMEND";
        String POPULAR = "POPULAR";
        String TOP_RATE = "TOP_RATE";
        String NOW_PLAYING = "NOW_PLAYING";
        String UPCOMING = "UPCOMING";
    }
}
