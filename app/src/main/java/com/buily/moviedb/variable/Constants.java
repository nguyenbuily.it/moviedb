package com.buily.moviedb.variable;

public class Constants {

    public static final String BASE_URL_MOVIE = "https://api.themoviedb.org/3/";
    public static final String API_KEY = "3956f50a726a2f785334c24759b97dc6";
    public static final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w185";
    public static final String PUT_OBJECT = "PUT_OBJECT";
    public static final String DEVELOP_KEY = "AIzaSyBW7DMM5L6er9Mmvc480oh7OuEunuD1o1A";
    public static final String PUT_TITLE = "PUT_TITLE";
    public static final String PUT_DATA = "PUT_DATA";
    public static final String FROM_TYPE = "FROM_TYPE";
}
